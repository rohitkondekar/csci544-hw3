import sys
import os
import json
import operator
import re
import codecs
import logging
import glob
import nltk


def POSTagger(inputFolderName):

	for filename in os.listdir(inputFolderName):
		if "." in filename:
			continue

		with open(inputFolderName+"/"+filename,'r',errors='ignore') as r_file:
			for line in r_file:
				result = ""
				tokens = nltk.word_tokenize(line.strip())

				new_list = []
				i = 0
				while i < len(tokens):
					if tokens[i] in ["'s","'re"]:
						new_list[len(new_list)-1]+=tokens[i]
					else:
						new_list.append(tokens[i])
					i+=1

				tups = nltk.pos_tag(new_list)
				for tup in tups:
					result+=" "+tup[0]
					result+="/"+tup[1]

				print(result.strip())

if __name__ == '__main__':
	POSTagger(sys.argv[1])