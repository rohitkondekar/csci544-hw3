import sys
import os
import json
import operator
import re
import codecs
import logging
import glob
import nltk
from percepclassify import percepclassify

def generateFeatures(inputFileName,modelFile):

	avgWeights = dict()
	with open(modelFile,'r') as r_file:
		avgWeights = json.load(r_file)

	# requiredList = ["its","it's","you're","your","their","they're","lose","loose","too","to","To","Too","Loose","Lose",
	# "It's","Its","Your","You're","Their","They're"]
	requiredList = ["its","it's","It's","Its","you're","your","Your","You're","lose","loose","Loose","Lose","Too","too","to","To",
	"Their","They're","their","they're"]
	typeOf = [1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5]
	
	with open(inputFileName,'r',errors='ignore') as r_file:
		for line in r_file:

			orig_list = line.strip().split()
			words = []
			tmp_list = nltk.word_tokenize(line.strip())
			
			new_list = []
			i = 0
			while i < len(tmp_list):
				if tmp_list[i] in ["'s","'re"] and tmp_list[i] not in orig_list:
					new_list[len(new_list)-1]+=tmp_list[i]
				else:
					new_list.append(tmp_list[i])
				i+=1

			tups = nltk.pos_tag(new_list)
			for tup in tups:
				words.append(tup[0])
				words.append(str.lower(tup[0]))
				# for t in tup:
					# words.append(t)

			index = 0
			result = []

			while index<len(words):
				
				# print(words[index]+"-----")
				if words[index] in requiredList:
					datapoint = []
					# datapoint.append("1:"+words[index])
					datapoint.append("1:"+str(typeOf[requiredList.index(words[index])]))

					#prev1words
					if index-2>=0:
						datapoint.append("2:"+words[index-2])
					else:
						datapoint.append("2:##empty##")

					#prev1Tag
					if index-1>=0:
						datapoint.append("3:"+words[index-1])
					else:
						datapoint.append("3:##empty##")

					#prev2words
					if index-4>=0:
						datapoint.append("4:"+words[index-4])
					else:
						datapoint.append("4:##empty##")

					#prev2Tag
					if index-4>=0:
						datapoint.append("5:"+words[index-3])
					else:
						datapoint.append("5:##empty##")

					#next1words
					if index+2<len(words):
						datapoint.append("6:"+words[index+2])
					else:
						datapoint.append("6:##empty##")

					#next1Tag
					if index+3<len(words):
						datapoint.append("7:"+words[index+3])
					else:
						datapoint.append("7:##empty##")

					#next2words
					if index+4<len(words):
						datapoint.append("8:"+words[index+4])
					else:
						datapoint.append("8:##empty##")

					#next2Tag
					if index+5<len(words):
						datapoint.append("9:"+words[index+5])
					else:
						datapoint.append("9:##empty##")

					#combiLast2Tag
					last1Tag = "##empty##"
					last2Tag = "##empty##"

					if index-1>=0:
						last1Tag = words[index-1]
					if index-3>=0:
						last2Tag = words[index-3]

					datapoint.append("10:"+last1Tag+last2Tag)

					#combiNext2Tag
					next1Tag = "##empty##"
					next2Tag = "##empty##"

					if index+3<len(words):
						next1Tag = words[index+3]
					if index+5<len(words):
						next2Tag = words[index+5]

					datapoint.append("11:"+next1Tag+next2Tag)
					words[index] = percepclassify(avgWeights,datapoint)
				
				result.append(words[index])			
				index+=2
			
			if len(result)==0:
				print("")
				continue

			finalOutput = ""
			o_index = 0
			r_index = 0

			# # print(orig_list)
			# # print(new_list)
			# print(new_list)

			# print(new_list)
			# print(orig_list)
			while o_index<len(orig_list) and r_index<len(new_list):
				if orig_list[o_index] == new_list[r_index]:
					if result[r_index] in requiredList:
						finalOutput+=" "+result[r_index]
					else:
						finalOutput+=" "+new_list[r_index]
					r_index+=1
					o_index+=1
				else:					
					tmp = ""
					tmp_r = ""
					while o_index<len(orig_list) and r_index<len(new_list) and tmp!=orig_list[o_index]:
							
						if result[r_index] in requiredList:
							tmp_r+=result[r_index]
						else:
							if new_list[r_index] == "``":
								new_list[r_index] = '"'
							elif new_list[r_index] == "''":
								new_list[r_index] = '"'
							tmp_r+=new_list[r_index]
						tmp+=new_list[r_index]
						r_index+=1
					o_index+=1
					finalOutput+=" "+tmp_r

			print(finalOutput.strip())
			sys.exit()

if __name__ == '__main__':
	generateFeatures(sys.argv[1],sys.argv[2])