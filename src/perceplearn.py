import sys
import os
import json
import operator
import time
import logging
import random
from percepclassify import percepclassify

############################################################
# Generic Perceptron Implementation
# Takes a feature matrix - contains words or something
# Datapoint is a list
############################################################

def percepLearn(dataPoints, classArray, modelFile="modelFile", numIterations=10, defaultClass = "NN"):

	#[feature][class] matrix
	weights = dict()
	avgWeights = dict()

	#Timestamp to save on computation time
	featureClassTimeStamp = dict()
	timeStamp = 0
	
	logging.debug("Number of iterations = "+str(numIterations))
	full_startTime = time.time()
	
	for it in range(0,numIterations):
		start_time = time.time()
		for rowIndex in range(0,len(dataPoints)):			
			timeStamp += 1

			#####Classify function inline to improve speed
			classScore = dict()
			maxClass = defaultClass #default class

			for feature in dataPoints[rowIndex]:
				if feature not in weights:
					continue
				for c in weights[feature]:
					classScore[c] = classScore.get(c,0) + weights[feature][c]
			#############

			#############
			#Get Max Scored Class
			if len(classScore)!=0:
				maxClass = max(classScore.items(), key=operator.itemgetter(1))[0]
			#############


			#############
			#mismatch
			#Averaging done only if mismatch
			#############

			if maxClass != classArray[rowIndex]:
				for feature in dataPoints[rowIndex]:
					if feature not in weights:
						weights[feature] = dict()
						avgWeights[feature] = dict()
						featureClassTimeStamp[feature] = dict()
					
					weights[feature][maxClass] = weights[feature].get(maxClass,0)-1
					weights[feature][classArray[rowIndex]] = weights[feature].get(classArray[rowIndex],0)+1
			 		
			 		#Avg with timeStamp addition - inside - only if change

			 		#for maxclass
					numItrs = timeStamp - featureClassTimeStamp[feature].get(maxClass,timeStamp)
					avgWeights[feature][maxClass] = avgWeights[feature].get(maxClass,0) + numItrs*(weights[feature][maxClass]+1)+weights[feature][maxClass]
					featureClassTimeStamp[feature][maxClass] = timeStamp

					#for otherclass
					numItrs = timeStamp - featureClassTimeStamp[feature].get(classArray[rowIndex],timeStamp)
					avgWeights[feature][classArray[rowIndex]] = avgWeights[feature].get(classArray[rowIndex],0) + numItrs*(weights[feature][classArray[rowIndex]]-1)+weights[feature][classArray[rowIndex]]
					featureClassTimeStamp[feature][classArray[rowIndex]] = timeStamp

		logging.debug("Iteration = " + str(it)+ " Time Spent = " + str(time.time()-start_time))
	#---- Finish of iterations

	#Final Averaging
	for feature in avgWeights:
		for c in avgWeights[feature]:
			numItrs = timeStamp - featureClassTimeStamp[feature][c]
			avgWeights[feature][c] = avgWeights[feature][c] + numItrs*(weights[feature][c])
			avgWeights[feature][c] /= numIterations

	logging.debug("Total Training Time = " + str(time.time()-full_startTime))
	#Write Model to a file
	with open(modelFile,'w') as w_file:
		json.dump(avgWeights,w_file)
	
	#Return Average weights for Development purposes
	return avgWeights

##############################
# Perceptron classifier for Development data
##############################
def getAccuracy(orig,output):

	#f_score calculation
	classDict = dict.fromkeys(set(orig))
	logging.debug(classDict.keys())
	for key in classDict:
		classDict[key] = dict()
		classDict[key]['belongs'] = orig.count(key)
		classDict[key]['classified_as'] = output.count(key)
		classDict[key]['correctly'] = 0

	correct = 0
	for i in range(0,len(orig)):
		if orig[i] == output[i]:
			correct = correct+1
			classDict[output[i]]['correctly']+=1

	#Print Fscore/Precision and Recall:
	for key in classDict:
		try:
			precision = classDict[key]['correctly']/classDict[key]['classified_as']
		except:
			precision = 0

		try:
			recall = classDict[key]['correctly']/classDict[key]['belongs']
		except:
			recall = 0

		try:
			fscore = (2*precision*recall)/(precision+recall)
		except:
			fscore = 0

		print("Precision for: "+key+" = "+str(precision))
		print("Recall for: "+key+" = "+str(recall))
		print("F-Score for: "+key+" = "+str(fscore))
		print("")

	return 100*correct/len(orig)




##############################
#Main method
##############################

if __name__ == '__main__':
	
	args = sys.argv[1:]
	
	#####################
	# Development Data Set
	#####################
	devFile = ""
	if "-h" in args:
		index = args.index("-h")
		devFile = args[index+1]
		del args[index]
		del args[index]


	# Debugging Information
	if "-v" in args:
		logging.basicConfig(stream=sys.stdout,level=logging.DEBUG)
		args.remove('-v')
	

	shuf = True
	# Debugging Information
	if "-deshuf" in args:
		args.remove('-deshuf')
		logging.debug("Not Shuffling Input")
		shuf = False
	else:
		logging.debug("Shuffling Input")
	

	# Shuffle Input - specifically for ham-spam
	lines = []
	with open(args[0],'r') as r_file:
		for line in r_file:
			lines.append(line.strip())

	if shuf:
		random.shuffle(lines)


	# DataStructure creation
	classArray = []
	dataPoints = []
	index = 0
	for line in lines:
		words = line.split()
		classArray.append(words[0])
		dataPoints.append([])
		for word in words[1:]:
				dataPoints[index].append(word)
		index = index+1


	# with open(args[0],'r') as r_file:
	# 	index = 0
	# 	for line in r_file:
	# 		words = line.strip().split()
	# 		classArray.append(words[0])
	# 		dataPoints.append([])
	# 		for word in words[1:]:
	# 			dataPoints[index].append(word)
	# 		index = index+1
	
	avgWeights = []

	logging.debug("----------------------------------------")
	logging.debug("Perceptron Training Started ...")
	
	try:
		avgWeights = percepLearn(dataPoints,classArray,args[1],int(args[2]))
	except Exception as e:
		avgWeights = percepLearn(dataPoints,classArray,args[1])

	logging.debug("Perceptron Training complete")
	logging.debug("----------------------------------------")


	# Testing on development set	
	if devFile != "":
		logging.debug("Development Dataset provided")
		with open(devFile,'r') as r_file:
			resultClass = []
			origClass = []
			for line in r_file:
				words = line.strip().split()
				origClass.append(words[0])
				resultClass.append(percepclassify(avgWeights,words[1:]))
			
			print("Accuracy = "+str(getAccuracy(origClass,resultClass)))
