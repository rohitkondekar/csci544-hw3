import sys
import os
import json
import operator
import re
import codecs
import logging
import glob

from perceplearn import percepLearn, percepclassify


def generateFeatures(inputDirectoryName):

	requiredList = ["its","it's","It's","Its","you're","your","Your","You're","lose","loose","Loose","Lose","Too","too","to","To",
	"Their","They're","their","they're"]
	typeOf = [1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5]
	featureMatrix = []
	classArray = []
	rowIndex = 0

	for filename in os.listdir(inputDirectoryName):
		if "." in filename:
			continue
		
		with open(inputDirectoryName+"/"+filename,'r',errors='ignore') as r_file:
			for line in r_file:

				words = re.split(r'[ \/]',line.strip())
				index = 0

				
				while index<len(words):
					
					if words[index] in requiredList:

						featureMatrix.append([])
						classArray.append(words[index])

						featureMatrix[rowIndex].append("1:"+str(typeOf[requiredList.index(words[index])]))

						#prev1words
						if index-2>=0:
							featureMatrix[rowIndex].append("2:"+words[index-2])
						else:
							featureMatrix[rowIndex].append("2:##empty##")

						#prev1Tag
						if index-1>=0:
							featureMatrix[rowIndex].append("3:"+str.lower(words[index-1]))
						else:
							featureMatrix[rowIndex].append("3:##empty##")

						#prev2words
						if index-4>=0:
							featureMatrix[rowIndex].append("4:"+words[index-4])
						else:
							featureMatrix[rowIndex].append("4:##empty##")

						#prev2Tag
						if index-4>=0:
							featureMatrix[rowIndex].append("5:"+str.lower(words[index-3]))
						else:
							featureMatrix[rowIndex].append("5:##empty##")

						#next1words
						if index+2<len(words):
							featureMatrix[rowIndex].append("6:"+words[index+2])
						else:
							featureMatrix[rowIndex].append("6:##empty##")

						#next1Tag
						if index+3<len(words):
							featureMatrix[rowIndex].append("7:"+str.lower(words[index+3]))
						else:
							featureMatrix[rowIndex].append("7:##empty##")

						#next2words
						if index+4<len(words):
							featureMatrix[rowIndex].append("8:"+words[index+4])
						else:
							featureMatrix[rowIndex].append("8:##empty##")

						#next2Tag
						if index+5<len(words):
							featureMatrix[rowIndex].append("9:"+str.lower(words[index+5]))
						else:
							featureMatrix[rowIndex].append("9:##empty##")

						#combiLast2Tag
						last1Tag = "##empty##"
						last2Tag = "##empty##"

						if index-1>=0:
							last1Tag = str.lower(words[index-1])
						if index-3>=0:
							last2Tag = str.lower(words[index-3])

						featureMatrix[rowIndex].append("10:"+last1Tag+last2Tag)

						#combiNext2Tag
						next1Tag = "##empty##"
						next2Tag = "##empty##"

						if index+3<len(words):
							next1Tag = str.lower(words[index+3])
						if index+5<len(words):
							next2Tag = str.lower(words[index+5])

						featureMatrix[rowIndex].append("11:"+next1Tag+next2Tag)
						
						rowIndex+=1

					index+=2
	return(classArray,featureMatrix)

if __name__ == '__main__':
	numItr = 10
	classArray, featureMatrix = generateFeatures(sys.argv[1])
	print(set(classArray))
	avgWeights = percepLearn(featureMatrix,classArray,sys.argv[2],int(numItr))


