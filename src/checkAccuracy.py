import sys
import os
import json
import operator
import re
import codecs
import logging
import glob
import nltk

def checkAccuracy(correctFile,testFile):

	requiredList = ["its","it's","It's","Its","you're","your","Your","You're","lose","loose","Loose","Lose","Too","too","to","To",
	"Their","They're","their","they're"]

	r1_lines = []
	r2_lines = []

	with open(correctFile,'r') as r1_file:
		for line in r1_file:
			r1_lines.append(line.strip())

	with open(testFile,'r') as r2_file:
		for line in r2_file:
			r2_lines.append(line.strip())

	total_count = 0
	correct = 0

	index = 0
	while index<len(r1_lines):
		words1 = r1_lines[index].split()
		words2 = r2_lines[index].split()

		i = 0
		while i < len(words1):
			print(words1[i]+" "+words2[i])
			if words1[i] in requiredList:
				total_count+=1
				if words1[i] == words2[i]:
					correct+=1
			i+=1
		index+=1
	print(correct)
	print(total_count)
	print(correct/total_count)

if __name__ == '__main__':
	checkAccuracy(sys.argv[1],sys.argv[2])

