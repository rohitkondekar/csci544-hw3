import sys
import os
import json
import operator

def percepclassify(weights,dataPoint,defaultClass="NN"):
	classScore = dict()
	for feature in dataPoint:
		if feature not in weights:
			continue
		for c in weights[feature]:
			classScore[c] = classScore.get(c,0) + weights[feature][c]

	#Default class
	if len(classScore)==0:
		return defaultClass

	maxClass = max(classScore.items(), key=operator.itemgetter(1))[0]
	return maxClass




#Main method
#======================================================

if __name__ == '__main__':
	avgWeights = dict()
	with open(sys.argv[1],'r') as r_file:
		avgWeights = json.load(r_file)
	
	for line in sys.stdin:
		words = line.strip().split()

		print(percepclassify(avgWeights,words))
		sys.stdout.flush()
