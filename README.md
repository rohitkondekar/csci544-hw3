# NLP Homework 3 #

### Method Used - Perceptron - from homework 2 ###

### Features used ###

```
* prev1words
* prev1Tag
* prev2words
* prev2Tag
* next1words
* next1Tag
* next2words
* next2Tag
* combiLast2Tag
* combiNext2Tag
```

### Files and there uses ###
```
* percepclassify - the average perceptron classifier
* perceplearn - the average perceptron trainer
* posTagger - pos tagger i am using to tag wiki set
* trainClassifier - used for training
* testClassifier - User to predict and fix words
* checkAccuracy.py - to get accuracy of dev set
```

### Accuracy on Dev set ###
```
Accuracy only for the given words - 97.4978%
```
### Words to fix###
```
"its","it's","It's","Its","you're","your","Your","You're","lose","loose","Loose","Lose","Too","too","to","To","Their","They're","their","they're"
```

###Libraries and methods used###
```
* nltk
* word_tokenize
* pos_tag
```